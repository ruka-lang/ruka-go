package main

import "fmt"
import "rukac/scanner"

type Error interface {
    kind() string
    msg() string
}

type compiler struct {
    errors []Error
    tokens []scanner.Token
}

type position struct {
    line int
    col int
}

type scanningError struct {
    message string
    location position
}

func (e scanningError) kind() string {
    return "Scanning error"
}

func (e scanningError) msg() string {
    return fmt.Sprintf("%s %d:%d %s", 
                        e.kind(), 
                        e.location.line, 
                        e.location.col, 
                        e.message,
                      )
}

func main() {
    c := compiler{}

    c.errors = append(c.errors, 
        scanningError{
            message: "Oh no",
            location: position{line: 1, col: 5},
        },
    )   

    c.errors = append(c.errors, 
        scanningError{
            message: "No oh",
            location: position{line: 2, col: 12},
        },
    )   

    for _, e := range c.errors {
        fmt.Println(e.msg())
    }
}
