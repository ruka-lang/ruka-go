package scanner

type TokenType string

type Token struct {
    Type TokenType
    Literal string
}

const (
    ILLEGAL = "ILLEGAL"
    EOF     = "EOF"

    IDENT = "IDENT"
    INT   = "INT"

    ASSIGN = "="
    PLUS   = "+"
    MINUS  = "-"
    BANG   = "!"
    ASTERISK = "*"
    SLASH  = "/"

    LT     = "<"
    GT     = ">"
    EQ     = "=="
    NOTEQ  = "!="

    COMMA  = ","
    SEMICOLON = ";"

    LPAREN = "("
    RPAREN = ")"
    LCURLY = "["
    RCURLY = "]"

    FUNCTION = "FUNCTION"
    LET      = "LET"
    IF       = "IF"
    ELSE     = "ELSE"
    TRUE     = "TRUE"
    FALSE    = "FALSE"
    RETURN   = "RETURN"
)

var keywords = map[string]TokenType {
    "fn": FUNCTION,
    "let": LET,
    "if": IF,
    "else": ELSE,
    "true": TRUE,
    "false": FALSE,
    "return": RETURN,
}

func LookupIdent(ident string) TokenType {
    if tok, ok := keywords[ident]; ok {
        return tok
    }

    return IDENT
}

func newToken(tt TokenType, lit string) Token {
    return Token{
        Type: tt,
        Literal: lit,
    }
}
