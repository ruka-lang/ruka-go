package scanner

type Scanner struct {
    input string
    position int
    readPosition int
    ch byte
}

func New(input string) *Scanner {
    s := &Scanner{input: input}
    s.readChar()
    return s
}

func (s *Scanner) readChar() {
    if s.readPosition >= len(s.input) {
        s.ch = 0
    } else {
        s.ch = s.input[s.readPosition]
    }

    s.position = s.readPosition
    s.readPosition += 1
}

func (s *Scanner) peekChar() byte {
    var ch byte
    if s.readPosition >= len(s.input) {
        ch = 0
    } else {
        ch = s.input[s.readPosition]
    }

    return ch
}

func (s *Scanner) readIdentifier() string {
    position := s.position
    for isLetter(s.ch) {
        s.readChar()
    }

    return s.input[position:s.position]
}

func (s *Scanner) readNumber() string {
    position := s.position
    for isDigit(s.ch) {
        s.readChar()
    }

    return s.input[position:s.position]
}

func (s *Scanner) skipWhitespace() {
    for s.ch == ' ' || s.ch == '\t' || s.ch == '\n' || s.ch == '\r' {
        s.readChar()
    }
}

func isLetter(ch byte) bool {
    return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_'
}

func isDigit(ch byte) bool {
    return '0' <= ch && ch <= '9'
}

func (s *Scanner) NextToken() Token {
    var tok Token

    s.skipWhitespace()
    switch s.ch {
        case '=':
            if s.peekChar() == '=' {
                tok = newToken(EQ, "==")
                s.readChar()
            } else {
                tok = newToken(ASSIGN, string(s.ch))
            }
        case ';':
            tok = newToken(SEMICOLON, string(s.ch))
        case '(':
            tok = newToken(LPAREN, string(s.ch))
        case ')':
            tok = newToken(RPAREN, string(s.ch))
        case ',':
            tok = newToken(COMMA, string(s.ch))
        case '+':
            tok = newToken(PLUS, string(s.ch))
        case '-':
            tok = newToken(MINUS, string(s.ch))
        case '/':
            tok = newToken(SLASH, string(s.ch))
        case '*':
            tok = newToken(ASTERISK, string(s.ch))
        case '<':
            tok = newToken(LT, string(s.ch))
        case '>':
            tok = newToken(GT, string(s.ch))
        case '!':
            if s.peekChar() == '=' {
                tok = newToken(NOTEQ, "!=")
                s.readChar()
            } else {
                tok = newToken(BANG, string(s.ch))
            }
        case '{':
            tok = newToken(LCURLY, string(s.ch))
        case '}':
            tok = newToken(RCURLY, string(s.ch))
        case 0:
            tok.Literal = ""
            tok.Type = EOF
        default: 
            if isLetter(s.ch) {
                tok.Literal = s.readIdentifier()
                tok.Type = LookupIdent(tok.Literal)
                return tok
            } else if isDigit(s.ch) {
                tok.Type = INT
                tok.Literal = s.readNumber()
                return tok
            } else {
                tok = newToken(ILLEGAL, string(s.ch))
            }
    }

    s.readChar()
    return tok
}
